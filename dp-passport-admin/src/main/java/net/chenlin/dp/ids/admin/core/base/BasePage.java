package net.chenlin.dp.ids.admin.core.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * bootstrap-table分页结果
 * @author zcl<yczclcn@163.com>
 */
public class BasePage<T> {

    /**
     * 查询结果
     */
    private List<T> rows;

    /**
     * 总条数
     */
    private long total;

    /**
     * constructor
     */
    public BasePage(){
        super();
    }

    /**
     * constructor
     * @param page
     */
    public BasePage(Page<T> page) {
        this.rows = page.getRecords();
        this.total = page.getTotal();
    }

    /**
     * getter for rows
     * @return
     */
    public List<T> getRows() {
        return rows;
    }

    /**
     * setter for rows
     * @param rows
     */
    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    /**
     * getter for total
     * @return
     */
    public long getTotal() {
        return total;
    }

    /**
     * setter for total
     * @param total
     */
    public void setTotal(long total) {
        this.total = total;
    }

}
